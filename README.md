# Product Hunt: api-node

This repo is a code-along in the [Rocketseat Starter Course](http://station.rocketseat.com.br/courses/starter).

## Project Setup

* Clone the Project - `git clone https://github.com/diegomais/rocketseat-producthunt-api-node.git`
* Install the dependencies - `npm install`
* Start the server - `npm start`

## Contributing

If you feel like there's a major problem, please open an issue to discuss the problem and potential resolution.

// Import the mongoose module: a mongodb object modeling for node.js.
const mongoose = require('mongoose');

// Mount a model from our compiled model Product.
const Product = mongoose.model('Product');

module.exports = {
  /** Retrieve all documents from products collection in the db.
   * Pagination defaults: page = 1 and limit per page = 10  */
  async index(req, res) {
    const { page = 1, limit = 10 } = req.query;
    const products = await Product.paginate({}, { page, limit });

    return res.json(products);
  },

  // Retrive with required document from products collection in the db.
  async show(req, res) {
    const product = await Product.findById(req.params.id);

    return res.json(product)
  },
  
  // Create a new document into products collection in the db.
  async store(req, res) {
    const product = await Product.create(req.body);

    return res.json(product);
  },

  /** Update a product with new data into products collection in the db.
   * { new: true }: That parameter make return new updated object instead the old object. */
  async update(req, res) {
    const product = await Product.findOneAndUpdate(req.params.id, req.body, { new: true });

    return res.json(product);
  },

  // Delete a product from products collection in the db.
  async destroy(req, res) {
    await Product.findOneAndRemove(req.params.id);

    return res.send();
  }
};
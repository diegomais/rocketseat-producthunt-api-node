// Import the Express module: a web application framework.
const express = require('express');

/** Create a modular, mountable route handlers.A Router instance is a complete middleware and
 * routing system; for this reason, it is often referred to as a “mini-app”.  */
const routes = express.Router();

// Mount the ProductController on the routes
const ProductController = require('./controllers/ProductController');

// Respond with all documents from products collection in the db.
routes.get('/products', ProductController.index);

// Respond with the required document from products collection in the db.
routes.get('/products/:id', ProductController.show);

// Create a document into products collection in the db.
routes.post('/products', ProductController.store);

// Update a product with new data into products collection in the db.
routes.put('/products/:id', ProductController.update);

// Delete a product from products collection in the db.
routes.delete('/products/:id', ProductController.destroy);

module.exports = routes;
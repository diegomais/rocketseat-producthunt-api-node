// Import the mongoose module: a mongodb object modeling for node.js.
const mongoose = require('mongoose');

// Import the mongoose-paginate module: a pagination plugin for mongoose.
const mongoosePaginate = require('mongoose-paginate');

/** Schema maps to a MongoDB collection and defines the shape of the documents within that
 * collection. Each key in our code ProductSchema defines a property in our documents which will
 * be cast to its associated SchemaType. For example, we've defined a property title which will
 * be cast to the String SchemaType and property date which will be cast to a Date SchemaType.
 * Keys may also be assigned nested objects containing further key/type definitions. */
const ProductSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  url: {
    type: String,
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
});

// Apply the mongoose-paginate module into ProductSchema.
ProductSchema.plugin(mongoosePaginate);

/** Models are fancy constructors compiled from Schema definitions. An instance of a model is
 * called a document. Models are responsible for creating and reading documents from the
 * underlying MongoDB database. The first argument is the singular name of the collection our
 * model is for. Mongoose automatically looks for the plural, lowercased version of our model name.
 * Thus, for the example below, the model Product is for the products collection in the database.
 * */
mongoose.model('Product', ProductSchema);
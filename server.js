// Import Express module: a web application framework.
const express = require('express');

// Import CORS module: middleware that can be used to enable CORS with various options.
const cors = require('cors');

// Import mongoose module: a mongodb object modeling for node.js.
const mongoose = require('mongoose');

// Import requireDir() module: a Node.js helper to require() directories.
const requireDir = require('require-dir');

// Create the app object by calling the top-level express() function exported by the Express module.
const app = express();

// Mount a middleware that parses incoming requests with JSON payloads.
app.use(express.json());

// Mount a middleware that enable all CORS requests.
app.use(cors());

/** Open a connection to the producthunt database running locally on the default port (27017).
 * useNewUrlParser: Enables the new, spec-compliant, url parser shipped in the MongoDB driver. */
mongoose.connect('mongodb://localhost:27017/producthunt', { useNewUrlParser: true });

// Make Mongoose use `findOneAndUpdate()`. Note that this option is `true`
// by default, you need to set it to false.
mongoose.set('useFindAndModify', false);

/** Create a connection constructor. For practical reasons, a Connection equals a Db.
 * Registering an error listener on the Connection lets us handle errors more locally.
 * Once our connection opens, our callback will be called. */
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'Connection error:'));
db.once('open', () => console.log('Database connected!'));

// Require the models directory.
requireDir('./src/models');

// Create a constant reference to port of remote server.
const port = 3001;

// Mount the routes on the app
app.use('/api', require('./src/routes'));

/** Bind and listen for connections on the specified port.
 * The app responds with “Hello World!” for requests to the root URL (/) or route.
 * For every other path, it will respond with a 404 Not Found. */
app.listen(port, () => console.log(`Example app listening on port ${port}!`));
